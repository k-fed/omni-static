console.log('
    { //TODO: remove this script tag
    //this script is just for demo. functionality to be added by AEM developer in the correct place
    $(document).ready(function () {
        function toggleArrow(e) {
            $(e.target)
                .prev('.panel-heading')
                .find("i.indicator")
                .toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
        }
        $('#category-nav').on('hidden.bs.collapse', toggleArrow);
        $('#category-nav').on('shown.bs.collapse', toggleArrow);
    });}
');
