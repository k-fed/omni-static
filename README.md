# Static modular Bootstrap / LESS build for OMNI Project.
====================================
A static, modular build using nunjucks for templating and gulp for tasks.

## Features

### LESS / CSS Stuff

- Watches for Less changes on save
- Checks for Less errors and outputs them without you having to rerun Gulp
- Autoprefixes for legacy browsers
- Combines all CSS into one big and sexy minified file
- Includes Less Bootstrap

### Javascript Stuff

- Automatically compiles all jQuery libraries into one big file JS file
- Lints custom scripts for errors
- Combines all custom scripts into one file

## How To Use

 run

    $ gulp

Or

    $ gulp server

...if you need a local server. Folder **build** serving at [http://localhost:8888](http://localhost:8888)

Should use [Livereload extension](http://livereload.com/extensions/). Or inject `<script src="//localhost:35729/livereload.js"></script>` into your page.

When you change a LESS(or JS) file, the page will reload.

## Templating

Main templates belong in the /templates folder. This is your html container... you can have multiple templates.

Individual pages go in /pages. Every new page should go in here and should be linked to its relavant template.

HTML components can be created in the templates/partials folder. Each component should have its own HTML file. These partials can be included in a page by using {% include "templates/partials/*.html" %} where * is the name of your HTML partial.

## Updates

**If this isn't working**, it's probably because you need to update. Just run `npm update --save-dev`

**If no command gulp found?**, you need to install it globally `npm install -g gulp` or run `npm run gulp`

### Still broken or not working?

Try this:

```javascript
sudo npm cache clean
npm install --save-dev
npm update --save-dev
gulp
```


## Quick Tips
- Any changes in `assets/less/*` will trigger the Less to compile on save
- All files in `assets/js/libs/*`  will be compressed to `build/jquery.plugins.min.js`
- All files in `assets/js/*` (except for `libs`) will be compressed to `build/scripts.min.js`


#### Special Thanks

To Kenny Song for his contributions and https://github.com/baivong
